package az.atl.demosecurity3.controller;

import az.atl.demosecurity3.model.client.ProductDto;
import az.atl.demosecurity3.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/product")
public class ProductController {
    private final ProductService productService;

    @GetMapping("/allProducts")
    public List<ProductDto> getAllProducts(@RequestHeader("Authorization") String token){
        return productService.getAllProducts(token);
    }

    @PostMapping("create")
    public void createProduct(@RequestHeader("Authorization") String token ,@RequestBody ProductDto productDto){
        productService.createProduct(token ,productDto);
    }
}
