package az.atl.demosecurity3.service;

import az.atl.demosecurity3.dao.entity.User;
import az.atl.demosecurity3.dao.repository.UserRepository;
import az.atl.demosecurity3.model.AuthenticationRequest;
import az.atl.demosecurity3.model.AuthenticationResponse;
import az.atl.demosecurity3.model.RegisterRequest;
import az.atl.demosecurity3.model.RegisterResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static az.atl.demosecurity3.model.RegisterResponse.buildRegisterDto;

@Service
public record AuthenticationService(UserRepository repository,
                                    PasswordEncoder passwordEncoder,
                                    JwtService jwtService,
                                    AuthenticationManager authenticationManager) {

    public RegisterResponse register(RegisterRequest request) {

        var isExist = repository.findByEmail(request.getEmail()).isPresent();
        if (isExist) {
            throw new RuntimeException("Email already exist!");
        }
        var user = User.builder()
                .fullName(request.getFullName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(request.getRole())
                .build();
        var userEntity = repository.save(user);

        return buildRegisterDto(userEntity);
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        var user = repository.findByEmail(request.getEmail())
                .orElseThrow(() -> new RuntimeException("User Not Found!"));
        var jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }
}

