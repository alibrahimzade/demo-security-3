package az.atl.demosecurity3.client;

import az.atl.demosecurity3.model.client.ProductDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(value = "productClient",url = "http://localhost:8083/product")
public interface ProductClient {
    @GetMapping("/allProducts")
    List<ProductDto> getAllProducts(@RequestHeader("Authorization") String token);

    @PostMapping("/create")
    void createProduct(@RequestHeader("Authorization") String token,ProductDto productDto);

}
