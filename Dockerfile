FROM amazoncorretto:17-alpine-jdk
MAINTAINER baeldung.com
COPY build/libs/demo-security-3-0.0.1-SNAPSHOT.jar docker-1.0.0.jar
ENTRYPOINT ["java","-jar","/docker-1.0.0.jar"]